---
title: "Master"
author: "Michael Rank"
date: "2023-09-01"
output:
  html_document:
    number_sections: true
    toc: true
    toc_depth: 2
    theme: cerulean
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(warning = FALSE)
```

Note: Run *installRpackages.R* in order to install all packages needed to compile this file.

```{r, include=F}
# Uncomment the following line and run, if you want to install all needed packages (checks for packages, you have installed already)
# source("installRpackages.R")
```


```{r packages, include=F}
# List of all needed packages
list.of.packages <- c("tidyverse", "sjPlot", "reshape2", "knitr", "sf", "colorspace", "data.table", "scales", "lme4", "e1071", "nnet", "patchwork")

# Load all packages
lapply(list.of.packages, require, character.only = TRUE)
```

```{r}
# Read hospital level consumption data from ADKA-if-DGI and remove data with missing federal states
consumption_hospital <- fread("Data/ADKAconsumptionData.csv", sep = ";")
consumption_hospital <- subset(consumption_hospital, !is.na(Bundesland))

# Read regional level resistance data from ARS-RKI
resistance_regional <- fread("Data/ARSresistanceData.csv", sep = ";")

# Create subsets of resistance data that will be needed in the following
resistance_regional_cefo <- subset(resistance_regional, substance == "Cefotaxim")
resistance_regional_cefo_2019 = subset(resistance_regional_cefo, year == 2019)
resistance_regional_cipro <- subset(resistance_regional, substance == "Ciprofloxacin")

# In-house UKD data
merged_ukd <- fread("Data/UKDcombinedData.csv", sep = ";")
```

```{r}
# Load some custom defined functions
source("myFunctions.R")
```

# Merge consumption and resistance data

## Aggregate consumption data to regional level

```{r}
consumption_regional <- aggregate_hospital_consumption_data()
```

```{r}
# Display a subset of the data
ind <- sample(nrow(consumption_regional), 5)
consumption_regional_sub <- consumption_regional[ind, ]
kable(consumption_regional_sub, row.names = F)
rm(ind, consumption_regional_sub)
```

## Merge data

```{r}
# Subsets of regional level resistance data for ciorofloxacin and cefotaxim
resistance_regional_cipro_cut <- subset(resistance_regional_cipro, select = c(R.all_materials, year.float, Bundesland, department, station_type))
resistance_regional_cefo_cut <- subset(resistance_regional_cefo, select = c(R.all_materials, year.float, Bundesland, department, station_type))

# Adjust the column names
names(resistance_regional_cipro_cut)[names(resistance_regional_cipro_cut) == "R.all_materials"] <- "R.cipro"
names(resistance_regional_cefo_cut)[names(resistance_regional_cefo_cut) == "R.all_materials"] <- "R.cefo"

# Merge consumption and resistance data
resistance_regional_merged <- merge(resistance_regional_cipro_cut, resistance_regional_cefo_cut, all = T)
merged_regional <- merge(resistance_regional_merged, consumption_regional)

# Order the rows of the merged data set
merged_regional <- merged_regional[order(merged_regional$Bundesland, merged_regional$department, merged_regional$station_type, merged_regional$year.float), ]
```

```{r}
# Display a subset of the data
ind <- sample(nrow(merged_regional), 5)
merged_regional_sub <- merged_regional[ind, ]
kable(merged_regional_sub)
rm(ind, merged_regional_sub)
```

# Figures & Numbers

## Figure 1a

```{r}
fig1a <- ggplot(consumption_hospital, aes(y = Antibiotika.100pd, x = year.float+2015)) +
  theme_white() +
  scale_y_continuous(limits = c(0,200)) +
  scale_x_continuous(breaks = 2015:2019) +
  geom_boxplot(aes(group = year.float)) +
  xlab(" ") + ylab("RDD/100pd")
fig1a
fig1a <- fig1a + ggtitle("a)")
```

## Figure 1b

```{r}
# Data set with consumption proportions of antibiotic classes, UKD
consumption_prop_ukd <- fread("Data/UKDclassPropData.csv", sep = ";")

fig1b <- ggplot(consumption_prop_ukd, aes(x = year.float+2015)) +
  geom_point(aes(y = value, color = abio_class)) +
  geom_line(aes(y = value, color = abio_class)) +
  theme_white() +
  xlab(" ") + ylab("Proportion of total\nantibiotic consumption") +
  scale_y_continuous(labels = scales::percent, limits = c(0, NA), expand = expansion(mult = c(0,0.05))) +
  theme(legend.position = c(0.5, 0.1))
fig1b
fig1b <- fig1b + ggtitle("b)")

ggsave("Figures/Figure_1.jpg", plot = fig1a + fig1b, dpi = 300, height = 5, width = 12)
```

## Consumption proportions regional

```{r}
# Data set with total consumption of antibiotic classes, all regions and hospitals
consumption_total <- fread("Data/ADKAclassData.csv", sep = ";")

consumption_total_2019 <- subset(consumption_total, year == 2019)
consumption_total_2015 <- subset(consumption_total, year == 2015)
```

### Proportions of fluoroquinolones & betalactams {-}

```{r}
(consumption_total_2019$fluoroquinolones + consumption_total_2019$betalactams) / consumption_total_2019$total
```

### Difference in fluoroquinolone proportions {-}

```{r}
consumption_total_2019$fluoroquinolones / consumption_total_2019$total - consumption_total_2015$fluoroquinolones / consumption_total_2015$total
```

### Difference in cephalosporin proportions {-}

```{r}
consumption_total_2019$cephalosporins / consumption_total_2019$total - consumption_total_2015$cephalosporins / consumption_total_2015$total
```

### Difference in penicillin proportions {-}

```{r}
consumption_total_2019$penicillins / consumption_total_2019$total - consumption_total_2015$penicillins / consumption_total_2015$total

rm(consumption_total_2019, consumption_total_2015)
```

## Figure 2a

```{r}
fig2a <- ggplot(resistance_regional_cipro, aes(y = R.all_materials, x = year.float+2015)) +
  theme_white() +
  scale_y_continuous(labels = scales::percent_format(scale = 1), limits = c(0,45)) +
  scale_x_continuous(breaks = 2015:2019) +
  geom_boxplot(aes(group = year.float)) +
  xlab(" ") + ylab("Proportion of resistant E. coli to ciprofloxacin\n(internal ARS database)")
fig2a
fig2a <- fig2a + ggtitle("a)")
```

## Figure 2b

```{r}
fig2b <- ggplot(resistance_regional_cefo, aes(y = R.all_materials, x = year.float+2015)) +
  theme_white() +
  scale_y_continuous(labels = scales::percent_format(scale = 1), limits = c(0,35)) +
  scale_x_continuous(breaks = 2015:2019) +
  geom_boxplot(aes(group = year.float)) +
  xlab(" ") + ylab("Proportion of resistant E. coli to cefotaxime\n(internal ARS database)")
fig2b
fig2b <- fig2b + ggtitle("b)")
```


## Resistance values regional

### Mean resistance to ciprofloxacin in 2015, first quarter {-}

```{r}
mean(resistance_regional_cipro$R.all_materials[resistance_regional_cipro$year.float == 0])
```

### Mean resistance to ciprofloxacin in 2017, second quarter {-}

```{r}
mean(resistance_regional_cipro$R.all_materials[resistance_regional_cipro$year.float == 2.25])
```

### Mean resistance to ciprofloxacin in 2019, fourth quarter {-}

```{r}
mean(resistance_regional_cipro$R.all_materials[resistance_regional_cipro$year.float == 4.75])
```

### Mean overall resistance to cefotaxime {-}

```{r}
mean(resistance_regional_cefo$R.all_materials)
```

## Figure 2c

```{r}
fig2c <- ggplot(merged_ukd, aes(y = R.cipro, x = year.float+2015)) +
  theme_white() +
  scale_y_continuous(labels = scales::percent_format(scale = 1), limits = c(0,50)) +
  scale_x_continuous(breaks = 2015:2019) +
  geom_boxplot(aes(group = year.float)) +
  xlab(" ") + ylab("Proportion of resistant E. coli to ciprofloxacin\n(UKD data)")
fig2c
fig2c <- fig2c + ggtitle("c)")
```

## Figure 2d

```{r}
fig2d <- ggplot(merged_ukd, aes(y = R.cefo, x = year.float+2015)) +
  theme_white() +
  scale_y_continuous(labels = scales::percent_format(scale = 1), limits = c(0,50)) +
  scale_x_continuous(breaks = 2015:2019) +
  geom_boxplot(aes(group = year.float)) +
  xlab(" ") + ylab("Proportion of resistant E. coli to cefotaxime\n(UKD data)")
fig2d
fig2d <- fig2d + ggtitle("d)")

ggsave("Figures/Figure_2.jpg", plot = fig2a + fig2b + fig2c + fig2d, dpi = 300, height = 10, width = 12)
```

## Consumption proportions UKD

```{r}
consumption_ukd <- fread("Data/UKDclassRDDData.csv", sep = ";")

consumption_ukd_2019 <- subset(consumption_ukd, year == 2019)
consumption_ukd_2015 <- subset(consumption_ukd, year == 2015)
```

### Difference in penicillin proportions {-}

```{r}
consumption_ukd_2019$penicillins / consumption_ukd_2019$total - consumption_ukd_2015$penicillins / consumption_ukd_2015$total
```

### Difference in fluoroquinolone proportions {-}

```{r}
consumption_ukd_2019$fluoroquinolones / consumption_ukd_2019$total - consumption_ukd_2015$fluoroquinolones / consumption_ukd_2015$total
```

### Difference in cephalosporin proportions {-}

```{r}
consumption_ukd_2019$cephalosporins / consumption_ukd_2019$total - consumption_ukd_2015$cephalosporins / consumption_ukd_2015$total
```

### Proportions of betalactams, penicillins & fluoroquinolones {-}

```{r}
consumption_ukd_2019$betalactams / consumption_ukd_2019$total
consumption_ukd_2019$penicillins / consumption_ukd_2019$total
consumption_ukd_2019$fluoroquinolones / consumption_ukd_2019$total

rm(consumption_ukd_2019, consumption_ukd_2015)
```

## Resistance values UKD

```{r}
merged_ukd_2019 <- subset(merged_ukd, year.float >= 4)
merged_ukd_2015 <- subset(merged_ukd, year.float < 1)
```

### Mean resistance to ciprofloxacin in 2015 {-}

```{r}
mean(merged_ukd_2015$R.cipro)
```

### Mean resistance to ciprofloxacin in 2019 {-}

```{r}
mean(merged_ukd_2019$R.cipro)
```

### Mean resistance to cefotaxime in 2015 {-}

```{r}
mean(merged_ukd_2015$R.cefo)
```

### Mean resistance to cefotaxime in 2019 {-}

```{r}
mean(merged_ukd_2019$R.cefo)

rm(merged_ukd_2019, merged_ukd_2015)
```

## Resistance variance values

### Variance of ciprofloxacin in UKD data {-}

```{r}
var(merged_ukd$R.cipro)
```
### Variance of ciprofloxacin in regional data {-}

```{r}
resistance_regional_cipro_sax <- subset(resistance_regional_cipro, Bundesland == "Sachsen")
var(resistance_regional_cipro_sax$R.all_materials)
rm(resistance_regional_cipro_sax)
```

### Variance of cefotaxime in UKD data {-}

```{r}
var(merged_ukd$R.cefo)
```
### Variance of cefotaxime in regional data {-}

```{r}
resistance_regional_cefo_sax <- subset(resistance_regional_cefo, Bundesland == "Sachsen")
var(resistance_regional_cefo_sax$R.all_materials)
rm(resistance_regional_cefo_sax)
```

# Linear models

## Ciprofloxacin

```{r}
# Linear mixed-effects model

lm.mixed <- lmer(R.cipro ~ 0 + sqrt(Ciprofloxacin) + sqrt(Moxifloxacin) + sqrt(Ceftriaxon) + sqrt(Flucloxacillin) + sqrt(Amoxicillin...Clavulansaeure) + sqrt(Cefotaxim) + sqrt(Gentamicin) + sqrt(Tobramycin) + sqrt(Roxithromycin) + sqrt(total_consumption) : department : station_type + log(patient_days) + (0 + total_consumption | Bundesland) + (0 + Ciprofloxacin | Bundesland), data = merged_regional, control = lmerControl(optimizer ='optimx', optCtrl=list(method='nlminb')))

# Display model output
tab_model(lm.mixed)
```

With a training/test split of 90%/10%, we obtain an average MSE of:

```{r}
# Calculate the mean MSE values
mean_mse_linear_model <- predict_mean_mse_linear_model_cipro(merged_regional, training_data_proportion = 0.9)

knitr::kable(mean_mse_linear_model)
```

Thereby, mean.value.mse corresponds to the error resulting by just guessing the average training resistance.

## Cefotaxime

```{r}
# Linear mixed-effects model
lm.mixed.cefo <- lmer(R.cefo ~ 0 + sqrt(Cefotaxim) + sqrt(Tobramycin) + sqrt(Gentamicin) + sqrt(Flucloxacillin) + sqrt(Piperacillin...Tazobactam) + sqrt(Metronidazol) + sqrt(Daptomycin) + sqrt(Vancomycin) + sqrt(Imipenem...Cilastatin) + sqrt(total_consumption) : department : station_type + (0 + total_consumption | Bundesland) + (0 + Cefotaxim | Bundesland), data = merged_regional, control = lmerControl(optimizer ='optimx', optCtrl=list(method='nlminb')))

# Display model output
tab_model(lm.mixed.cefo)
```

With a training/test split of 90%/10%, we obtain an average MSE of:

```{r}
# Calculate the mean MSE values
mean_mse_linear_model_cefo <- predict_mean_mse_linear_model_cefo(merged_regional, training_data_proportion = 0.9)

knitr::kable(mean_mse_linear_model_cefo)
```

# UKD linear model

```{r}
# Adjust the data structure
merged_ukd_cut <- merged_ukd[, -(41:51)]
merged_ukd_cut$station_type <- "normal ward"
merged_ukd_cut$station_type[merged_ukd_cut$ICU == 1] <- "ICU"
merged_ukd_cut$department <- "internal medicine"
merged_ukd_cut$department[merged_ukd_cut$surgery == 1] <- "surgery"
merged_ukd_cut <- merged_ukd_cut[, -c(41:44)]
```

```{r}
# Linear model for in-house UKD data
lm.ukd <- lm(R.cipro ~ 0 + sqrt(Ciprofloxacin) : department : station_type + sqrt(Levofloxacin) + sqrt(Moxifloxacin) + sqrt(Cefotaxim) + sqrt(Gentamicin) + sqrt(total_consumption), data = merged_ukd_cut)

tab_model(lm.ukd)
```

# Artificial neural network

## Generating the data set

```{r}
# Reorder the data
merged_regional <- merged_regional[with(merged_regional, order(Bundesland, year.float, department, station_type))]

# Remove substances that have less than 0.1 RDD/100pd 
substance_column_names_low <- substance_column_names[colMeans(subset(merged_regional, select = substance_column_names), na.rm = T) <= 0.1]
drop_columns <- !(names(merged_regional) %in% substance_column_names_low)
merged_regional_neural <- merged_regional[, ..drop_columns]

# Perform one-hot-encoding of the non-numerical variables (necessary for most machine learning algorithms)
merged_regional_neural <- cbind(merged_regional_neural, class.ind(merged_regional_neural$Bundesland))
merged_regional_neural <- cbind(merged_regional_neural, class.ind(merged_regional$department))
merged_regional_neural <- cbind(merged_regional_neural, class.ind(merged_regional$station_type))
merged_regional_neural <- subset(merged_regional_neural, select = -c(Bundesland, department, station_type, year, quarter, length.of.stay))
merged_regional_neural <- data.frame(merged_regional_neural)

# Restructure the columns
merged_regional_neural <- relocate(merged_regional_neural, year.float, .after = Penicillin.V)
merged_regional_neural <- relocate(merged_regional_neural, R.cipro, .after = R.cefo)
merged_regional_neural <- relocate(merged_regional_neural, cases, .after = patient_days)

# Create neural net data set
merged_regional_neural <- subset(merged_regional_neural, !is.na(R.cipro) & !is.na(R.cefo))

# merged_regional_neural <- merged_regional_neural[with(merged_regional_neural, order(Baden.Wuerttemberg, Bayern, Berlin, Brandenburg..Mecklenburg.Vorpommern, Bremen..Niedersachsen, Hamburg..Schleswig.Holstein, Hessen, Nordrhein.Westfalen, Rheinland.Pfalz..Saarland, Sachsen.Anhalt..Thueringen, Sachsen, year.float, internal.medicine, surgery, ICU, normal.ward)), ]

# Reorder the rows
merged_regional_neural <- merged_regional_neural[with(merged_regional_neural, order(Sachsen, Sachsen.Anhalt..Thueringen, Rheinland.Pfalz..Saarland, Nordrhein.Westfalen, Hessen, Hamburg..Schleswig.Holstein, Bremen..Niedersachsen, Brandenburg..Mecklenburg.Vorpommern, Berlin, Bayern, Baden.Wuerttemberg, year.float, surgery, internal.medicine, normal.ward, ICU)), ]

# Adjust some names
names(merged_regional_neural)[names(merged_regional_neural) == "patient_days"] <- "patient.days"
names(merged_regional_neural)[names(merged_regional_neural) == "Penicillin.G...Benzylpenicillin."] <- "Penicillin.G"

# Save the data set to make it available to Python scripts
write.table(merged_regional_neural, file = "Data/MLTrainingData.csv", quote = F, sep = ";", row.names = F)
```

Saved as *MLTrainingData.csv*.

## Python implementation

The artificial neural network (ANN) is implemented in Python. See the files *neural_combo.py*, *neural_helper.py* and *neural_tuner.py*. Run *neural_combo.py* to obtain results and diagnostics of the used ANN. You can find the details of the hyperparameter tuning in *neural_tuner.py*.