# Limitations of estimating antibiotic resistance using German hospital consumption data - A comprehensive computational analysis

Data and code for the publication: "Limitations of estimating antibiotic resistance using German hospital consumption data - A comprehensive computational analysis" by M. Rank, A. Kather, D. Wilke, M. Steib-Bauert, W. V. Kern, I. Röder, K. de With

## Table of Contents

- [Description](#description)
- [Files](#files)
- [Data](#data)
- [Usage](#usage)
- [Support](#support)
- [License](#license)

## Description

For almost a century, antibiotics have played an important role in the treatment of infectious diseases. However, the efficacy of these very drugs is now threatened by the development of resistances, which pose major challenges to medical professionals and decision-makers. Thereby, the consumption of antibiotics in hospitals is an important driver that can be targeted directly. To illuminate the relation between consumption and resistance depicts a very important step in this procedure. With the help of comprehensive ecological and clinical data, we applied a variety of different computational approaches ranging from classical linear regression to artificial neural networks to analyze antibiotic resistance in Germany. These mathematical and statistical models demonstrate that the amount and particularly the structure of currently available data sets lead to contradictory results and do, therefore, not allow for profound conclusions. More effort and attention on both data collection and distribution is necessary to overcome this problem. In particular, our results suggest that at least monthly or quarterly antibiotic use and resistance data at the department and ward level for each hospital (including application route and type of specimen) are needed to reliably determine the extent to which antibiotic consumption influences resistance development.


## Files

- `Master.Rmd`: Main R code file.
- `installRpackages.R`: Check missing R packages and install them.
- `myFunctions.R`: Auxiliary functions for Master.Rmd.
- `neural_combo.py`: Main Python code file for ANN.
- `neural_helper.py`: Auxiliary functions.
- `neural_tuner.py`: Hyperparameter tuning for ANN.

## Data

All data is included in the [data folder](Data):

- ADKA-IF-DGI project:
    - `ADKAclassData.csv`: total consumption in RDD for some antibiotic classes
    - `ADKAconsumptionData.csv`: hospital level consumption data
- ARS-RKI project:
    - `ARSresistanceData.csv`: regional level resistance data
- internal UKD data:
    - `UKDclassPropData.csv`: relative consumption for some antibiotic classes
    - `UKDclassRDDData.csv`: total consumption in RDD for some antibiotic classes
    - `UKDcombinedData.csv`: hospital level consumption and resistance data

## Usage

### 1. R Usage

1. Open and run `installRpackages.R`, if you have unfulfilled package dependencies.
2. Open `Master.Rmd` in RStudio or any R environment.
3. Run the code chunks sequentially to reproduce the findings and all plots of the puclication. It will also create the data set that is needed for the ANN specified in Python code

### 2. Python Usage

0. Run `Master.Rmd` first!
1. Open and run `neural_tuner.py` to perform the hyperparameter tuning for the ANN.
2. Open and run `neural_combo.py` to recontruct the ANN described in the publication.

## Support

For support please contact michael.rank@tu-dresden.de

## License

Antibiotic resistance Rank et al © 2024 by Michael Rank is licensed under CC BY 4.0.

