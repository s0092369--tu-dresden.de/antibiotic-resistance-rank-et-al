# -*- coding: utf-8 -*-

########################## Evaluate artificial neural net #####################

# =============================================================================
# Note: In order to obtain the needed data set, run "Master.Rmd" first.
# =============================================================================

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import tensorflow as tf
import random

from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam

import eli5
from eli5.sklearn import PermutationImportance
# from keras.wrappers.scikit_learn import KerasRegressor

from sklearn.model_selection import train_test_split

import plotly.express as px
from plotly.offline import plot

import neural_helper as nh

############################

n_zero_lines = 2 # number of added zero lines to force zero intercept
    
################ Read the data:

data_merged = pd.read_csv("Data/MLTrainingData.csv", sep = ";")

X = data_merged.iloc[:,2:]
y = data_merged.iloc[:,0:2]    

## Best results of Cipro model:
X = X.drop(['cases','patient.days', 'Clindamycin', 'Cotrimoxazol', 'Linezolid', 'Metronidazol', 'Ampicillin', 'Daptomycin', 'Imipenem...Cilastatin', 'Meropenem', 'Clarithromycin', 'Doxycyclin', 'Cefpodoxim', 'Tigecyclin', 'Piperacillin...Tazobactam', 'Fosfomycin', 'Azithromycin', 'Erythromycin', 'year.float'], axis = 1)
n_total = len(X.columns)
substance_names = X.columns


############## Get average MSE

print("Fitting model with " + str(n_zero_lines) + " additional zero line(s).")
# X_train, X_test, Y_train, Y_test = nh.average_mse(X, y, n_zero_lines=n_zero_lines)
random.seed(19)    
X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.1)   


############## Add zero lines to final model

X_train_z, Y_train_z = nh.add_zero_lines(X_train, Y_train, n_zero_lines=n_zero_lines)


############## Build model
    
net = nh.optimal_regression_model(n_total, n_zero_lines=n_zero_lines, seed=19)
history = net.fit(X_train_z, Y_train_z, epochs=100, validation_data=(X_test, Y_test), batch_size=32, verbose=0)
print("\n Building model...")
loss = history.history
    
fig, ax = plt.subplots()
ax.plot(loss['loss'], label="Training")
ax.plot(loss['val_loss'], label="Test")
ax.set_xlabel('Epochs')
ax.set_ylabel('Mean Squared Error')
ax.legend()
plt.show()

test_mse = net.evaluate(X_test, Y_test, verbose=0)
print("\nMSE: " + str(test_mse)) 


############################ Evaluate test cases 

print("\nApply the neural net for some test cases.")         

### Calculate means

avg_consumptions = X.mean(axis = 0)
X_avg_raw = np.zeros((1,n_total))
X_avg_raw[0,:] = avg_consumptions
X_zero_avg = X_avg_raw.copy()
try:
    X_zero_avg[0,0:np.where(substance_names == "patient.days")[0][0]] = 0
except:
    X_zero_avg[0,0:np.where(substance_names == "Baden.Wuerttemberg")[0][0]] = 0

X_avg = np.zeros((1,n_total))
X_avg[0,:] = avg_consumptions
print('Prediction for average amounts:' + str(nh.predict_mean_avg(net,X_avg, substance_names)))  

### Average total consumption

# total = np.mean(X_train["total_consumption"])  
total = np.mean(X["total_consumption"])  

### Test cases
    
X_zero = X_zero_avg.copy()
print('Prediction for zero amounts:' + str(nh.predict_mean_avg(net, X_zero, substance_names)))   
    
X_catastrophy = X_zero_avg.copy()
X_catastrophy[0,np.where(substance_names == "total_consumption")[0][0]] = 3*total # Total
total_fluour_cepha = avg_consumptions["Moxifloxacin"] + avg_consumptions["Levofloxacin"] + avg_consumptions["Ciprofloxacin"] + avg_consumptions["Cefuroxim"] + avg_consumptions["Cefotaxim"] + avg_consumptions["Ceftriaxon"] + avg_consumptions["Cefazolin"] + avg_consumptions["Ceftazidim"]
X_catastrophy[0,np.where(substance_names == "Ciprofloxacin")[0][0]] = avg_consumptions["Ciprofloxacin"]/total_fluour_cepha*3*total # Ciprofloxacin
X_catastrophy[0,np.where(substance_names == "Levofloxacin")[0][0]] = avg_consumptions["Levofloxacin"]/total_fluour_cepha*3*total # Levofloxacin
X_catastrophy[0,np.where(substance_names == "Moxifloxacin")[0][0]] = avg_consumptions["Moxifloxacin"]/total_fluour_cepha*3*total # Moxifloxacin
X_catastrophy[0,np.where(substance_names == "Cefuroxim")[0][0]] = avg_consumptions["Cefuroxim"]/total_fluour_cepha*3*total # Cefuroxim
X_catastrophy[0,np.where(substance_names == "Cefotaxim")[0][0]] = avg_consumptions["Cefotaxim"]/total_fluour_cepha*3*total # Cefotaxim
X_catastrophy[0,np.where(substance_names == "Ceftriaxon")[0][0]] = avg_consumptions["Ceftriaxon"]/total_fluour_cepha*3*total # Ceftriaxon
X_catastrophy[0,np.where(substance_names == "Cefazolin")[0][0]] = avg_consumptions["Cefazolin"]/total_fluour_cepha*3*total # Cefazolin
X_catastrophy[0,np.where(substance_names == "Ceftazidim")[0][0]] = avg_consumptions["Ceftazidim"]/total_fluour_cepha*3*total # Ceftazidim
print('Prediction for catastrophy case:' + str(nh.predict_mean_avg(net, X_catastrophy, substance_names)))    
    
X_peni = X_zero_avg.copy()
total_peni = avg_consumptions["Amoxicillin"] + avg_consumptions["Ampicillin...Sulbactam"] + avg_consumptions["Penicillin.G"] + avg_consumptions["Penicillin.V"] + avg_consumptions["Flucloxacillin"] + avg_consumptions["Amoxicillin...Clavulansaeure"]
X_peni[0,np.where(substance_names == "Amoxicillin")[0][0]] = avg_consumptions["Amoxicillin"]/total_peni*total # Amoxicillin
X_peni[0,np.where(substance_names == "Ampicillin...Sulbactam")[0][0]] = avg_consumptions["Ampicillin...Sulbactam"]/total_peni*total # Ampicillin/Sulbactam
X_peni[0,np.where(substance_names == "Penicillin.G")[0][0]] = avg_consumptions["Penicillin.G"]/total_peni*total # Penicillin G
X_peni[0,np.where(substance_names == "Penicillin.V")[0][0]] = avg_consumptions["Penicillin.V"]/total_peni*total # Penicillin V
X_peni[0,np.where(substance_names == "Flucloxacillin")[0][0]] = avg_consumptions["Flucloxacillin"]/total_peni*total # Flucloxacillin
X_peni[0,np.where(substance_names == "Amoxicillin...Clavulansaeure")[0][0]] = avg_consumptions["Amoxicillin...Clavulansaeure"]/total_peni*total # Amoxicillin/Clavulansaeure
# X_peni[0,np.where(substance_names == "Piperacillin...Tazobactam")[0][0]] = 0.3*total # Pip/Taz
X_peni[0,np.where(substance_names == "total_consumption")[0][0]] = total # Total
print('Prediction for Penicillin case:' + str(nh.predict_mean_avg(net, X_peni, substance_names)))

X_cepha = X_zero_avg.copy()
total_cepha = avg_consumptions["Cefuroxim"] + avg_consumptions["Cefotaxim"] + avg_consumptions["Ceftriaxon"] + avg_consumptions["Cefazolin"] + avg_consumptions["Ceftazidim"]
X_cepha[0,np.where(substance_names == "Cefuroxim")[0][0]] = avg_consumptions["Cefuroxim"]/total_cepha*total # Cefuroxim
X_cepha[0,np.where(substance_names == "Cefotaxim")[0][0]] = avg_consumptions["Cefotaxim"]/total_cepha*total # Cefotaxim
X_cepha[0,np.where(substance_names == "Ceftriaxon")[0][0]] = avg_consumptions["Ceftriaxon"]/total_cepha*total # Ceftriaxon
X_cepha[0,np.where(substance_names == "Cefazolin")[0][0]] = avg_consumptions["Cefazolin"]/total_cepha*total # Cefazolin
X_cepha[0,np.where(substance_names == "Ceftazidim")[0][0]] = avg_consumptions["Ceftazidim"]/total_cepha*total # Ceftazidim
X_cepha[0,np.where(substance_names == "total_consumption")[0][0]] = total # Total
print('Prediction for Cephalosporin case:' + str(nh.predict_mean_avg(net, X_cepha, substance_names)))

X_fluour = X_zero_avg.copy()
total_fluour = avg_consumptions["Moxifloxacin"] + avg_consumptions["Levofloxacin"] + avg_consumptions["Ciprofloxacin"]
X_fluour[0,np.where(substance_names == "Ciprofloxacin")[0][0]] = avg_consumptions["Ciprofloxacin"]/total_fluour*total # Ciprofloxacin
X_fluour[0,np.where(substance_names == "Levofloxacin")[0][0]] = avg_consumptions["Levofloxacin"]/total_fluour*total # Levofloxacin
X_fluour[0,np.where(substance_names == "Moxifloxacin")[0][0]] = avg_consumptions["Moxifloxacin"]/total_fluour*total # Moxifloxacin
X_fluour[0,np.where(substance_names == "total_consumption")[0][0]] = total # Total
print('Prediction for Fluouroquinolone case:' + str(nh.predict_mean_avg(net, X_fluour, substance_names)))  
 

########## Plot MSE VS number of zero lines

# nh.plot_mse_VS_zeros(X_train, Y_train, X_test, Y_test, max_zeros=20, which_model=1)


######### Compare to UKD data

data_UKD = pd.read_csv("Data/UKDcombinedData.csv", sep = ";", encoding='latin-1')
X_ukd = data_UKD.iloc[:,2:]
y_ukd = data_UKD.iloc[:,0:2]    
X_ukd = X_ukd.drop(['cases','patient.days', 'Clindamycin', 'Cotrimoxazol', 'Linezolid', 'Metronidazol', 'Ampicillin', 'Daptomycin', 'Imipenem...Cilastatin', 'Meropenem', 'Clarithromycin', 'Doxycyclin', 'Cefpodoxim', 'Tigecyclin', 'Piperacillin...Tazobactam', 'Fosfomycin', 'Azithromycin', 'Erythromycin', 'year.float'], axis = 1)
print("MSE of just guessing the average resistance: " + str(round(np.mean(np.mean(pow(y_ukd-np.mean(y),2))),2)))

# net.evaluate(X_ukd, y_ukd)

tmp = net.predict(X_ukd)
np.mean(np.mean(pow(y_ukd-tmp,2)))

print('\nMean squared errors for model applied to in-house UKD data:')
mses_ukd = []
for n in [0,1,2,3,5,10]:
    mses_ukd.append(nh.average_mse_ukd(X, y, n_zero_lines=n))


########### Predicted VS Observed Plot for Saxony

X_test_Saxony = X_test.loc[X_test['Sachsen'] == 1]
Y_test_Saxony = Y_test.loc[X_test['Sachsen'] == 1]
Y_test_Saxony = Y_test_Saxony.reset_index(drop=True)
Y_model_Saxony = pd.DataFrame(net.predict(X_test_Saxony), columns=['R.cefo','R.cipro'], dtype='float')

observed_vs_predicted = pd.DataFrame({'observed': Y_test_Saxony['R.cefo'],'predicted': Y_model_Saxony['R.cefo']}, dtype='float')
observed_vs_predicted.to_csv("observed_vs_predicted_neural_net_Saxony.csv", sep=";", index=False)

Y_model = pd.DataFrame(net.predict(X_test), columns=['R.cefo','R.cipro'], dtype='float')
observed_vs_predicted = pd.DataFrame({'observed': Y_test.reset_index(drop=True)['R.cefo'],'predicted': Y_model['R.cefo']}, dtype='float')
observed_vs_predicted.to_csv("observed_vs_predicted_neural_net.csv", sep=";", index=False)

fig = px.scatter(data_frame=observed_vs_predicted, x='observed', y='predicted')
fig.show()
# plot(fig)











