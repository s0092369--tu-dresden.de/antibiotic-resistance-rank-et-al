# -*- coding: utf-8 -*-

############### Helper functions for artificial neural net ####################

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# import matplotlib.ticker as mtick
import tensorflow as tf
import random

from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam

import eli5
from eli5.sklearn import PermutationImportance
from keras.wrappers.scikit_learn import KerasRegressor

from sklearn.model_selection import train_test_split

### Return the best neural net regression model according the number of zero lines

def optimal_regression_model(n_total, n_zero_lines=0, seed=42):
    random.seed(seed)
    tf.compat.v1.random.set_random_seed(seed)
    np.random.seed(seed)
    if n_zero_lines == 0:
        #### Original model:
        net = Sequential([
            Dense(units=50, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
            Dense(units=50, activation='relu'),
            Dense(units=50, activation='relu'),
            Dense(units=30, activation='relu'),
            Dense(units=2, activation='linear')])
            # Compile model
        net.compile(loss='mean_squared_error', optimizer=Adam(lr=0.001))
        return net
        # ### Keras-tuner model choice for no zero-lines:
        # net = Sequential([
        #     Dense(units=50, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
        #     Dense(units=50, activation='relu'),
        #     Dense(units=20, activation='relu'),
        #     Dense(units=45, activation='relu'),
        #     Dense(units=2, activation='linear')])
        # # Compile model
        # net.compile(loss='mean_squared_error', optimizer=Adam(lr=0.001))
        # return net
    elif n_zero_lines == 1:
        #### Keras-tuner model choice for one zero-line:
        # net = Sequential([
        #     Dense(units=50, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
        #     Dense(units=40, activation='relu'),
        #     Dense(units=10, activation='relu'),
        #     Dense(units=45, activation='relu'),
        #     Dense(units=30, activation='relu'),
        #     Dense(units=2, activation='linear')])
        #### Repeated Keras-tuner model choice for one zero-line -> 2nd Best model so far
        net = Sequential([
            Dense(units=50, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
            Dense(units=50, activation='relu'),
            Dense(units=35, activation='relu'),
            Dense(units=45, activation='relu'),
            Dense(units=45, activation='relu'),
            Dense(units=2, activation='linear')])
        # Compile model
        net.compile(loss='mean_squared_error', optimizer=Adam(lr=0.001))
        return net
    elif n_zero_lines == 2:
        #### Keras-tuner model choice for two zero-lines:
        # net = Sequential([
        #     Dense(units=50, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
        #     Dense(units=40, activation='relu'),
        #     Dense(units=30, activation='relu'),
        #     Dense(units=45, activation='relu'),
        #     Dense(units=30, activation='relu'),
        #     Dense(units=2, activation='linear')])
        #### Repeated Keras-tuner model choice for two zero-line -> Best model so far
        # net = Sequential([
        #     Dense(units=25, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
        #     Dense(units=30, activation='relu'),
        #     Dense(units=35, activation='relu'),
        #     Dense(units=2, activation='linear')])
        # net = Sequential([
        #     Dense(units=50, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
        #     Dense(units=25, activation='relu'),
        #     Dense(units=30, activation='relu'),
        #     Dense(units=35, activation='relu'),
        #     Dense(units=2, activation='linear')])
        net = Sequential([
            Dense(units=50, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
            Dense(units=50, activation='relu'),
            Dense(units=30, activation='relu'),
            Dense(units=2, activation='linear')])
        # Compile model
        net.compile(loss='mean_squared_error', optimizer=Adam(lr=0.001))
        return net
    elif n_zero_lines == 3:
        #### Keras-tuner model choice for three zero-lines:
        # net = Sequential([
        #     Dense(units=50, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
        #     Dense(units=20, activation='relu'),
        #     Dense(units=25, activation='relu'),
        #     Dense(units=20, activation='relu'),
        #     Dense(units=50, activation='relu'),
        #     Dense(units=2, activation='linear')])
        #### Repeated Keras-tuner model choice for three zero-line 
        net = Sequential([
            Dense(units=30, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
            Dense(units=25, activation='relu'),
            Dense(units=50, activation='relu'),
            Dense(units=2, activation='linear')])
        # Compile model
        net.compile(loss='mean_squared_error', optimizer=Adam(lr=0.001))
        return net
    elif n_zero_lines == 5:
        #### Repeated Keras-tuner model choice for five zero-line -> okay...
        net = Sequential([
            Dense(units=50, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
            Dense(units=50, activation='relu'),
            Dense(units=45, activation='relu'),
            Dense(units=2, activation='linear')])
        # Compile model
        net.compile(loss='mean_squared_error', optimizer=Adam(lr=0.001))
        return net
    elif n_zero_lines == 10:
        #### Repeated Keras-tuner model choice for ten zero-line -> quite good (3rd best)
        net = Sequential([
            Dense(units=50, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
            Dense(units=35, activation='relu'),
            Dense(units=45, activation='relu'),
            Dense(units=45, activation='relu'),
            Dense(units=2, activation='linear')])
        # Compile model
        net.compile(loss='mean_squared_error', optimizer=Adam(lr=0.001))
        return net
    else:
        print("No optimal model determined for " + str(n_zero_lines) + " added zero lines. Use original model.")
        net = Sequential([
            Dense(units=50, input_dim=n_total, activation='relu', kernel_initializer='he_uniform'),
            Dense(units=50, activation='relu'),
            Dense(units=50, activation='relu'),
            Dense(units=30, activation='relu'),
            Dense(units=2, activation='linear')])
        # Compile model
        net.compile(loss='mean_squared_error', optimizer=Adam(lr=0.001))
        return net
    
### Add zero lines to data

def add_zero_lines(X_train, Y_train, n_zero_lines=0):
    X_train_z = X_train.copy()
    Y_train_z = Y_train.copy()
    for i in range(n_zero_lines):
        zero_line_rd = pd.Series(0, index=X_train_z.columns)
        zero_line_y = pd.Series(0, index=Y_train_z.columns)
        ### Add random station type, department and region
        station = random.choice(["ICU", "normal.ward"])
        zero_line_rd[station] = 1
        BL = random.choice(['Baden.Wuerttemberg', 'Bayern', 'Berlin','Brandenburg..Mecklenburg.Vorpommern', 'Bremen..Niedersachsen','Hamburg..Schleswig.Holstein', 'Hessen', 'Nordrhein.Westfalen','Rheinland.Pfalz..Saarland', 'Sachsen', 'Sachsen.Anhalt..Thueringen'])
        zero_line_rd[BL] = 1
        dep = random.choice(['internal.medicine', 'surgery'])
        zero_line_rd[dep] = 1
        X_train_z = X_train_z.append(zero_line_rd, ignore_index=True)
        Y_train_z = Y_train_z.append(zero_line_y, ignore_index=True)
    return [X_train_z, Y_train_z]
 
### Calculate average MSE

def average_mse(X, y, n_zero_lines=0):  
    mses = list()
    for r_s in range(20):
        random.seed(r_s)
        X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.1)
        n_total = len(X_train.columns)
        net = optimal_regression_model(n_total, n_zero_lines=n_zero_lines, seed=r_s)
        # Add zero lines
        X_train_z, Y_train_z = add_zero_lines(X_train, Y_train, n_zero_lines=0)
        # Fit model
        net.fit(X_train_z, Y_train_z, epochs=100, validation_data=(X_test, Y_test), batch_size=32, verbose=0)
        tmp = net.evaluate(X_test, Y_test, verbose=0)
        mses.append(tmp)
    print("Average MSE of UKD data for " + str(n_zero_lines) + " added zero lines: " + str(np.mean(mses))) 
    return [X_train, X_test, Y_train, Y_test]

### Calculate average MSE of UKD reference data

def average_mse_ukd(X, y, n_zero_lines=0):  
    data_UKD = pd.read_csv("Data/UKDcombinedData.csv", sep = ";", encoding='latin-1')
    X_ukd = data_UKD.iloc[:,2:]
    y_ukd = data_UKD.iloc[:,0:2]    
    X_ukd = X_ukd.drop(['cases','patient.days', 'Clindamycin', 'Cotrimoxazol', 'Linezolid', 'Metronidazol', 'Ampicillin', 'Daptomycin', 'Imipenem...Cilastatin', 'Meropenem', 'Clarithromycin', 'Doxycyclin', 'Cefpodoxim', 'Tigecyclin', 'Piperacillin...Tazobactam', 'Fosfomycin', 'Azithromycin', 'Erythromycin', 'year.float'], axis = 1)
     
    mses = list()
    for r_s in range(20):
        random.seed(r_s)
        X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.1)
        n_total = len(X_train.columns)
        net = optimal_regression_model(n_total, n_zero_lines=n_zero_lines, seed=r_s)
        # Add zero lines
        X_train_z, Y_train_z = add_zero_lines(X_train, Y_train, n_zero_lines=0)
        # Fit model
        net.fit(X_train_z, Y_train_z, epochs=100, validation_data=(X_test, Y_test), batch_size=32, verbose=0)
        tmp = net.evaluate(X_ukd, y_ukd, verbose=0)
        mses.append(tmp)
    print(str(n_zero_lines) + " additional zero lines. Average MSE: " + str(np.mean(mses)))
    return np.mean(mses)
  
### Average prediction routines

def predict_mean(net, X_test, substance_names, department="internal.medicine", station_type="normal.ward"):
    BL_list = ['Baden.Wuerttemberg', 'Bayern', 'Berlin','Brandenburg..Mecklenburg.Vorpommern', 'Bremen..Niedersachsen','Hamburg..Schleswig.Holstein', 'Hessen', 'Nordrhein.Westfalen','Rheinland.Pfalz..Saarland', 'Sachsen', 'Sachsen.Anhalt..Thueringen']
    prediction_values_cefo = []
    prediction_values_cipro = []
    
    if department == "surgery":
        X_test[0,np.where(substance_names == "surgery")[0][0]] = 1
        X_test[0,np.where(substance_names == "internal.medicine")[0][0]] = 0
    elif department == "internal.medicine":
        X_test[0,np.where(substance_names == "surgery")[0][0]] = 0
        X_test[0,np.where(substance_names == "internal.medicine")[0][0]] = 1
    else:
        print("Error: Department not supported...")
        return
    if station_type == "ICU":
        X_test[0,np.where(substance_names == "ICU")[0][0]] = 1
        X_test[0,np.where(substance_names == "normal.ward")[0][0]] = 0
    elif station_type == "normal.ward":
        X_test[0,np.where(substance_names == "ICU")[0][0]] = 0
        X_test[0,np.where(substance_names == "normal.ward")[0][0]] = 1
    else:
        print("Error: Station type not supported...")
        return
    
    for BL in BL_list:
        X_test[0,np.where(substance_names == BL)[0][0]] = 1
        for not_BL in BL_list:
            if not_BL != BL:
                X_test[0,np.where(substance_names == not_BL)[0][0]] = 0
        tmp_cefo = net.predict(X_test)[0][0]
        tmp_cipro = net.predict(X_test)[0][1]
        prediction_values_cefo.append(tmp_cefo)
        prediction_values_cipro.append(tmp_cipro)
    return [round(np.mean(prediction_values_cefo),2) , round(np.mean(prediction_values_cipro),2)]

def predict_mean_str(net, X_test, substance_names):
    return_str = " "
    for dep in ["surgery", "internal.medicine"]:
        for stat in ["ICU", "normal.ward"]:
            predicted_value = predict_mean(net, X_test, department=dep, station_type=stat, substance_names=substance_names)
            return_str = return_str + dep[0:8] + "/" + stat[0:6] + ":" + str(predicted_value) + ", "
    return return_str

def predict_mean_avg(net, X_test, substance_names):
    cefos = []
    cipros = []
    for dep in ["surgery", "internal.medicine"]:
        for stat in ["ICU", "normal.ward"]:
            predicted_values = predict_mean(net, X_test, department=dep, station_type=stat, substance_names=substance_names)
            cefos.append(predicted_values[0])
            cipros.append(predicted_values[1])
    return_cefo = round(np.mean(cefos), 2)
    return_cipro = round(np.mean(cipros), 2)
    return str(" Cefo " + str(return_cefo) + ", Cipro " + str(return_cipro))

################ Correlation of number of zero lines and MSE:
  
def plot_mse_VS_zeros(X_train, Y_train, X_test, Y_test, max_zeros=20, which_model=0):
    n_total = len(X_train.columns)
    mses = list()
    for n_zero_lines in range(max_zeros+1):
        X_train_z = X_train.copy()
        Y_train_z = Y_train.copy()
        for i in range(n_zero_lines):
            zero_line_rd = pd.Series(0, index=X_train_z.columns)
            zero_line_z = pd.Series(0, index=Y_train_z.columns)
            station = random.choice(["ICU", "normal.ward"])
            zero_line_rd[station] = 1
            BL = random.choice(['Baden.Wuerttemberg', 'Bayern', 'Berlin','Brandenburg..Mecklenburg.Vorpommern', 'Bremen..Niedersachsen','Hamburg..Schleswig.Holstein', 'Hessen', 'Nordrhein.Westfalen','Rheinland.Pfalz..Saarland', 'Sachsen', 'Sachsen.Anhalt..Thueringen'])
            zero_line_rd[BL] = 1
            dep = random.choice(['internal.medicine', 'surgery'])
            zero_line_rd[dep] = 1
            
            X_train_z = X_train_z.append(zero_line_rd, ignore_index=True)
            Y_train_z = Y_train_z.append(zero_line_z, ignore_index=True)
            
        net = optimal_regression_model(n_total, n_zero_lines=which_model, seed=42)
        net.fit(X_train_z, Y_train_z, epochs=100, validation_data=(X_test, Y_test), batch_size=32, verbose=0)
        tmp = net.evaluate(X_test, Y_test, verbose=0)
        mses.append(tmp)
    
    fig, ax = plt.subplots()
    ax.plot(mses)
    ax.set_xlabel('Number of zeros')
    ax.set_ylabel('Mean Squared Error')
    plt.show()
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    