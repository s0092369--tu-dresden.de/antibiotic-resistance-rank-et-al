# -*- coding: utf-8 -*-

############## Find optimal parameters for artificial neural net ##############

# =============================================================================
# Note: In order to obtain the needed data set, run "Master.Rmd" first.
# =============================================================================

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# import matplotlib.ticker as mtick
import tensorflow as tf
import random
import sys

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam

import keras_tuner as kt

import eli5
from eli5.sklearn import PermutationImportance
from tensorflow.keras.wrappers.scikit_learn import KerasRegressor

from sklearn.model_selection import train_test_split

import neural_helper as nh

tmp = input("Specify the number of zero lines: ")
try:
    n_zero_lines = int(tmp)
    print("\nTuning model with " + tmp + " zero lines.")
except:
    sys.exit("ERROR: Please provide an integer number!")

################ Read the data:

data_merged = pd.read_csv("Data/MLTrainingData.csv", sep = ";")

X = data_merged.iloc[:,2:]
y = data_merged.iloc[:,0:2]    

## Best results of Cipro model:
X = X.drop(['cases','patient.days', 'Clindamycin', 'Cotrimoxazol', 'Linezolid', 'Metronidazol', 'Ampicillin', 'Daptomycin', 'Imipenem...Cilastatin', 'Meropenem', 'Clarithromycin', 'Doxycyclin', 'Cefpodoxim', 'Tigecyclin', 'Piperacillin...Tazobactam', 'Fosfomycin', 'Azithromycin', 'Erythromycin', 'year.float'], axis = 1)

random.seed(42)    
X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.1)   
substance_names = X_train.columns
n_total = len(X_train.columns)

############# Tune model performance

print("\n############# Tune model performance ################# \n")

def model_builder(hp):
  model = Sequential()
  # model.add(Dense(units=hp.Int('start_unit', min_value=5, max_value=50, step=5),
  #                 input_dim=n_total,
  #                 activation='relu'))
  model.add(Dense(units=50, input_dim=n_total, activation='relu'))
  for i in range(hp.Int('num_layers', 1, 4)):
      model.add(Dense(units = hp.Int('units_' + str(i), min_value=5, max_value=50, step=5),
                      activation='relu'))
  model.add(Dense(units=2, activation='linear'))
  hp_learning_rate = hp.Choice('learning_rate', values=[1e-3, 1e-4])
  model.compile(loss='mean_squared_error', optimizer=Adam(learning_rate=hp_learning_rate),metrics=['mse'])
  # model.compile(loss='mean_squared_error', optimizer=Adam(learning_rate=0.001),metrics=['mse'])
  return model

# Instantiate the tuner
tf.keras.backend.set_floatx('float64') # To avoid error message

# tuner = kt.Hyperband(model_builder, objective='val_loss', max_epochs=100,
#                       directory='keras_tuning_log', project_name='khyperband',
#                       overwrite=True)

tuner = kt.Hyperband(model_builder, objective='val_loss', max_epochs=100,
                      directory='keras_tuning_log', project_name='khyperband',
                      overwrite=True, executions_per_trial=20)

# print("")
# tuner.search_space_summary() 

# X_train_z = X_train.copy()
# Y_train_z = Y_train.copy()
# for i in range(n_zero_lines):
#     zero_line_rd = pd.Series(0, index=X_train_z.columns)
#     zero_line_y = pd.Series(0, index=Y_train_z.columns)
#     ### Add random station type, department and region
#     station = random.choice(["ICU", "normal.ward"])
#     zero_line_rd[station] = 1
#     BL = random.choice(['Baden.Wuerttemberg', 'Bayern', 'Berlin','Brandenburg..Mecklenburg.Vorpommern', 'Bremen..Niedersachsen','Hamburg..Schleswig.Holstein', 'Hessen', 'Nordrhein.Westfalen','Rheinland.Pfalz..Saarland', 'Sachsen', 'Sachsen.Anhalt..Thueringen'])
#     zero_line_rd[BL] = 1
#     dep = random.choice(['internal.medicine', 'surgery'])
#     zero_line_rd[dep] = 1
#     X_train_z = X_train_z.append(zero_line_rd, ignore_index=True)
#     Y_train_z = Y_train_z.append(zero_line_y, ignore_index=True)

X_z = X.copy()
y_z = y.copy()
X_train_z = X_train.copy()
Y_train_z = Y_train.copy()
for i in range(n_zero_lines):
    zero_line_rd = pd.Series(0, index=X_z.columns)
    zero_line_y = pd.Series(0, index=y_z.columns)
    ### Add random station type, department and region
    station = random.choice(["ICU", "normal.ward"])
    zero_line_rd[station] = 1
    BL = random.choice(['Baden.Wuerttemberg', 'Bayern', 'Berlin','Brandenburg..Mecklenburg.Vorpommern', 'Bremen..Niedersachsen','Hamburg..Schleswig.Holstein', 'Hessen', 'Nordrhein.Westfalen','Rheinland.Pfalz..Saarland', 'Sachsen', 'Sachsen.Anhalt..Thueringen'])
    zero_line_rd[BL] = 1
    dep = random.choice(['internal.medicine', 'surgery'])
    zero_line_rd[dep] = 1
    X_z = X_z.append(zero_line_rd, ignore_index=True)
    y_z = y_z.append(zero_line_y, ignore_index=True)
    X_train_z = X_train_z.append(zero_line_rd, ignore_index=True)
    Y_train_z = Y_train_z.append(zero_line_y, ignore_index=True)


print("")
# tuner.search(X_train_z, Y_train_z, epochs=100, validation_data=(X_test, Y_test), batch_size=32, verbose=0)
tuner.search(X_z, y_z, epochs=100, validation_split=0.1, batch_size=32, verbose=0)

print("")
tuner.results_summary()
# best_hp=tuner.get_best_hyperparameters()
print("")
best_hp=tuner.get_best_hyperparameters(num_trials=1)[0]

h_model = tuner.hypermodel.build(best_hp)
# h_model = tuner.get_best_models(num_models=2)[1]
h_model.fit(X_train_z, Y_train_z, epochs=100, validation_data=(X_test, Y_test), batch_size=32, verbose=0)
h_model.summary()
h_mse = h_model.evaluate(X_test, Y_test, verbose=0)

print(h_mse)